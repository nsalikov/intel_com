# -*- coding: utf-8 -*-
import re
import scrapy
from scrapy.spiders import SitemapSpider


class PdfSpider(SitemapSpider):
    name = 'pdf'
    allowed_domains = ['intel.com']

    sitemap_urls = ['https://www.intel.com/sitemap.xml']
    sitemap_rules = [('us/en/products', 'parse')]


    def parse(self, response):
        htmls = [response.urljoin(url) for url in response.css('#productPortfolio-1 a ::attr(href)').extract() if '.html' in url]
        #pdfs = [response.urljoin(url) for url in response.css('#pdfAssetDetail-1 a ::attr(href)').extract() if '.pdf' in url]
        pdfs = [response.urljoin(url) for url in response.css('a ::attr(href)').extract() if '.pdf' in url]

        path = ['https://www.intel.com/sitemap.xml', response.url]

        if pdfs:
            d = {}

            d['url'] = response.url
            d['path'] = path
            d['file_urls'] = pdfs

            yield d

        for url in htmls:
            yield response.follow(url, meta={'path': path}, callback=self.parse_one_level_deeper)


    def parse_one_level_deeper(self, response):
        htmls = [response.urljoin(url) for url in response.css('#productPortfolio-1 a ::attr(href)').extract() if '.html' in url]
        #pdfs = [response.urljoin(url) for url in response.css('#pdfAssetDetail-1 a ::attr(href)').extract() if '.pdf' in url]
        pdfs = [response.urljoin(url) for url in response.css('a ::attr(href)').extract() if '.pdf' in url]

        path = response.meta['path'] + [response.url]

        if pdfs:
            d = {}

            d['url'] = response.url
            d['path'] = path
            d['file_urls'] = pdfs

            yield d
